package com.poker.analyzers;

import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;

/**
 * Analyzer to determine the winner.
 *
 */
public interface HandAnalyzer {

    /**
     * Get the poker hand type being analyzed
     * @return the poker hand type being analyzed
     */
    PokerHandTypeEnum getPokerHandType();

    /**
     * Determine if the two players has the poker hand being analyzed for.
     * @param hand1 player1 hand
     * @param hand2 player2 hand
     * @return returns the game result with the winning hand if any, else returns {@link GameResult#NO_WINNER}
     */
    GameResult determineGameResult(PlayerHand hand1, PlayerHand hand2);
}
