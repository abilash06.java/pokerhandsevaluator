/**
 *
 */

package com.poker.analyzers;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;

/**
 * Analyzer for {@link PokerHandTypeEnum#HIGH_CARD}
 */
public class HighCardAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.HIGH_CARD;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return true;
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {
        return breakTieUsingAllCards(hand1, hand2);
    }

    @Override
    protected void printResult(final GameResult gameResult){
        final String message = String.format("Player %s wins. - with %s: high card %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName(),
            gameResult.getHighCard().getFace());
        System.out.println(message);
    }
}
