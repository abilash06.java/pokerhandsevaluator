package com.poker.analyzers;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import com.poker.util.PokerHandUtils;

/**
 * Analyzer for {@link PokerHandTypeEnum#FOUR_OF_A_KIND}
 */
public class FourOfAKindAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.FOUR_OF_A_KIND;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        return PokerHandUtils.findCardWithFrequency(hand, 4);
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return PokerHandUtils.isFourOfAKind(hand.getValuesWithCountMap());
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {
        return breakTieUsingWinningCard(hand1, hand2);
    }

    @Override
    protected void printResult(final GameResult gameResult) {
        final String message = String.format("Player %s wins. - with %s: %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName(),
            getWinningCard(gameResult.getWinningHand()).getFace());
        System.out.println(message);
    }
}
