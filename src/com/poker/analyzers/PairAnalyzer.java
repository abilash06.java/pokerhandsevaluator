package com.poker.analyzers;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import com.poker.util.PokerHandUtils;

/**
 * Analyzer for {@link PokerHandTypeEnum#PAIR}
 */
public class PairAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.PAIR;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        return PokerHandUtils.findCardWithFrequency(hand, 2);
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return PokerHandUtils.isPair(hand.getValuesWithCountMap());
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {

        final CardValueEnum winningCardHand1 = getWinningCard(hand1);
        final CardValueEnum winningCardHand2 = getWinningCard(hand2);
        GameResult gameResult = null;

        // if the pair cards have the same value for both the players, use the remaining cards to break the tie.
        if (winningCardHand1.getValue() == winningCardHand2.getValue()) {
            gameResult = breakTieUsingAllCards(hand1, hand2);
            // the tie breaker will print the result
        } else if (winningCardHand1.getValue() > winningCardHand2.getValue()) {
            gameResult = new GameResult(hand1, getPokerHandType());
            printResult(gameResult);
        } else {
            gameResult = new GameResult(hand2, getPokerHandType());
            printResult(gameResult);
        }

        return gameResult;
    }

    @Override
    protected void printResult(final GameResult gameResult){
        final String message = String.format("Player %s wins. - with %s: %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName(),
            PokerHandUtils.findCardWithFrequency(gameResult.getWinningHand(), 2).getFace());
        System.out.println(message);
    }

}
