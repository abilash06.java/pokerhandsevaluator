/**
 *
 */

package com.poker.analyzers;

import java.util.Collections;
import java.util.List;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;

/**
 * Abstract call to hold the common code of all the analyzers.
 *
 */
public abstract class AbstractPokerCardAnalyzer implements HandAnalyzer {

    /** indicator for tie */
    public static final boolean TIE = true;

    @Override
    public GameResult determineGameResult(final PlayerHand hand1, final PlayerHand hand2) {

        final boolean hand1Won = isWinner(hand1);
        final boolean hand2Won = isWinner(hand2);

        if (!hand1Won && !hand2Won) {
            return GameResult.NO_WINNER;
        }

        final GameResult gameResult;
        if (hand1Won && hand2Won) {
            gameResult = breakTie(hand1, hand2);
            // the breakTie method will take care of printing the result.
        } else if (hand1Won) {
            gameResult = new GameResult(hand1, getPokerHandType());
            printResult(gameResult);
        } else {
            gameResult = new GameResult(hand2, getPokerHandType());
            printResult(gameResult);
        }

        return gameResult;
    }

    /**
     * Get the card that decided the winner
     * @param hand the player hand
     * @return the winning card
     */
    protected abstract CardValueEnum getWinningCard(PlayerHand hand);

    /**
     * Check if the hand has this specific poker hand; used to indicate the winner
     * @param hand the player hand
     * @return true if the input hand won
     */
    protected abstract boolean isWinner(PlayerHand hand);

    /**
     * Helps break the tie if both the hands has the same poker hand
     * @param hand1 player1 hand
     * @param hand2 player2 hand
     * @return result after resolving the tie
     */
    protected abstract GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2);

    /**
     * Print the winning result in a readable format
     * @param gameResult gameResult of the winning hand
     */
    protected abstract void printResult(GameResult gameResult);

    /**
     * Break the tie using only the winning card.
     * Examples: {@link PokerHandTypeEnum#FOUR_OF_A_KIND}, {@link PokerHandTypeEnum#FULL_HOUSE} and so on.
     * @param hand1 player1 hand
     * @param hand2 player2 hand
     * @return game result after breaking the tie.
     */
    protected GameResult breakTieUsingWinningCard(final PlayerHand hand1, final PlayerHand hand2) {

        final CardValueEnum winningCardHand1 = getWinningCard(hand1);
        final CardValueEnum winningCardHand2 = getWinningCard(hand2);
        final GameResult gameResult;

        if (winningCardHand1.getValue() == winningCardHand2.getValue()) {
            gameResult = new GameResult(getPokerHandType(), TIE);
            printResultForTie();
        } else if (winningCardHand1.getValue() > winningCardHand2.getValue()) {
            gameResult = new GameResult(hand1, getPokerHandType());
            printResult(gameResult);
        } else {
            gameResult = new GameResult(hand2, getPokerHandType());
            printResult(gameResult);
        }

        return gameResult;
    }

    /**
     * Break the tie using multiple cards in the player hands.
     * Examples: {@link PokerHandTypeEnum#FLUSH}, {@link PokerHandTypeEnum#HIGH_CARD} and so on.
     * @param hand1 player1 hand
     * @param hand2 player2 hand
     * @return game result after breaking the tie.
     */
    protected GameResult breakTieUsingAllCards(final PlayerHand hand1, final PlayerHand hand2) {

        final List<CardValueEnum> allCardValuesHand1 = hand1.getCardValues();
        Collections.sort(allCardValuesHand1);
        final List<CardValueEnum> allCardValuesHand2 = hand2.getCardValues();
        Collections.sort(allCardValuesHand2);

        GameResult gameResult = null;
        // compare corresponding elements in the ordered lists to find the winning hand using high card.
        // starting from the last card assuming the assuming the list is in ASC order.
        for (int i = 4; i >= 0; i--) {
            final CardValueEnum cardValueHand1 = allCardValuesHand1.get(i);
            final CardValueEnum cardValueHand2 = allCardValuesHand2.get(i);

            final boolean winnerFound;
            if (cardValueHand1.getValue() > cardValueHand2.getValue()) {
                gameResult = new GameResult(hand1, getPokerHandType(), cardValueHand1);
                winnerFound = true;
            } else if (cardValueHand1.getValue() < cardValueHand2.getValue()) {
                gameResult = new GameResult(hand2, getPokerHandType(), cardValueHand2);
                winnerFound = true;
            } else {
                winnerFound = false;
            }

            if (winnerFound) {
                break;
            }
        }

        if (gameResult == null) {// both the hands won => tie
            gameResult = new GameResult(getPokerHandType(), TIE);
            printResultForTie();
        } else {
            printResult(gameResult);
        }

        return gameResult;
    }

    /**
     * Print result when game tie.
     */
    private static void printResultForTie() {
        System.out.println("Tie.");
    }

}
