/**
 *
 */

package com.poker.analyzers;

import java.util.Collections;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import com.poker.util.PokerHandUtils;

/**
 * Analyzer for {@link PokerHandTypeEnum#STRAIGHT_FLUSH}
 */
public class StraightFlushAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.STRAIGHT_FLUSH;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        return Collections.max(hand.getCardValues());
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return PokerHandUtils.isFlush(hand.getSuitsWithCountMap()) && PokerHandUtils.isStraight(hand.getCardValues());
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {
        return breakTieUsingWinningCard(hand1, hand2);
    }

    @Override
    protected void printResult(final GameResult gameResult){
        final String message = String.format("Player %s wins. - with %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName());
        System.out.println(message);
    }
}
