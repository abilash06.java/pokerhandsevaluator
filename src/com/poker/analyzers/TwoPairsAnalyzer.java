package com.poker.analyzers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import com.poker.util.PokerHandUtils;

/**
 * Analyzer for {@link PokerHandTypeEnum#TWO_PAIRS}
 */
public class TwoPairsAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.TWO_PAIRS;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        final List<CardValueEnum> pairEntries = hand.getValuesWithCountMap().entrySet().stream().filter(t -> t.getValue().equals(2)).map(Map.Entry::getKey)
            .collect(Collectors.toList());

        final CardValueEnum highCardValue = Collections.max(pairEntries, Comparator.comparingInt(CardValueEnum::getValue));
        return highCardValue;
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return PokerHandUtils.haveTwoPairs(hand.getValuesWithCountMap());
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {
        return breakTieUsingAllCards(hand1, hand2);
    }

    @Override
    protected void printResult(final GameResult gameResult){
        final String pair = PokerHandUtils.findCardsWithFrequency(gameResult.getWinningHand(), 2)
            .stream().map( n -> String.valueOf(n.getFace())).collect(Collectors.joining(","));
        final String message = String.format("Player %s wins. - with %s: %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName(),
            pair);
        System.out.println(message);
    }
}
