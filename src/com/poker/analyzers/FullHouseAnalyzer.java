package com.poker.analyzers;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import com.poker.util.PokerHandUtils;

/**
 * Analyzer for {@link PokerHandTypeEnum#FULL_HOUSE}
 */
public class FullHouseAnalyzer extends AbstractPokerCardAnalyzer {

    @Override
    public PokerHandTypeEnum getPokerHandType() {
        return PokerHandTypeEnum.FULL_HOUSE;
    }

    @Override
    protected CardValueEnum getWinningCard(final PlayerHand hand) {
        return PokerHandUtils.findCardWithFrequency(hand, 3);
    }

    @Override
    protected boolean isWinner(final PlayerHand hand) {
        return (PokerHandUtils.isThreeOfAKind(hand.getValuesWithCountMap()) && PokerHandUtils.isPair(hand.getValuesWithCountMap()));
    }

    @Override
    protected GameResult breakTie(final PlayerHand hand1, final PlayerHand hand2) {
        return breakTieUsingWinningCard(hand1, hand2);
    }

    @Override
    protected void printResult(final GameResult gameResult){
        final String message = String.format("Player %s wins. - with %s: %s over %s", gameResult.getWinningHand().getPlayerName(), getPokerHandType().getName(),
            PokerHandUtils.findCardWithFrequency(gameResult.getWinningHand(), 3).getFace(),
            PokerHandUtils.findCardWithFrequency(gameResult.getWinningHand(), 2).getFace());
        System.out.println(message);
    }
}
