package com.poker.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.PlayerHand;

/**
 * Helper methods for all the analyzers
 */
public final class PokerHandUtils {

    /**
     * default constructor
     */
    private PokerHandUtils() {
        // prevent instantiation
    }

    /**
     * Check if the map contains a card suit with count 5 (flush)
     * @param suitsMap map of all the card suits
     * @return true if flush; else false
     */
    public static boolean isFlush(final Map<CardSuitEnum, Integer> suitsMap) {
        return suitsMap.values().contains(5);
    }

    /**
     * Check if the input list of values are in a sequence (straight)
     * @param values all the card values
     * @return true if straight; else false
     */
    public static boolean isStraight(final List<CardValueEnum> values) {
        final List<CardValueEnum> list = new ArrayList<>(values);
        Collections.sort(list);

        int straightCount = 0;
        for (int i = 0; i < 4; i++) {
            if (list.get(i).getValue() + 1 == list.get(i + 1).getValue()) {
                straightCount++;
            } else {
                break;
            }
        }
        return straightCount == 4;
    }

    /**
     * Check if map contains a card value with count 4 (four of a kind)
     * @param valuesMap map of all the card values
     * @return true of four of a kind; else false
     */
    public static boolean isFourOfAKind(final Map<CardValueEnum, Integer> valuesMap) {
        return hasValueCount(valuesMap, 4);
    }

    /**
     * Check if map contains a card value with count 3 (three of a kind)
     * @param valuesMap map of all the card values
     * @return true of three of a kind; else false
     */
    public static boolean isThreeOfAKind(final Map<CardValueEnum, Integer> valuesMap) {
        return hasValueCount(valuesMap, 3);
    }

    /**
     * Check if map contains a pair of cards
     * @param valuesMap map of all the card values
     * @return true if pair; else false
     */
    public static boolean isPair(final Map<CardValueEnum, Integer> valuesMap) {
        return hasValueCount(valuesMap, 2);
    }

    /**
     * Check if map contains two pair of cards
     * @param valuesMap map of all the card values
     * @return true if two pairs; else false
     */
    public static boolean haveTwoPairs(final Map<CardValueEnum, Integer> valuesMap) {
        return valuesMap.entrySet().stream().filter(t -> t.getValue().equals(2)).count() == 2;
    }

    /**
     * Find a first card that is in the hand with the frequency specified
     * @param hand player hand
     * @param frequency count to check
     * @return card value
     */
    public static CardValueEnum findCardWithFrequency(final PlayerHand hand, final int frequency) {
        final List<CardValueEnum> result = findCardsWithFrequency(hand, frequency);
        return result.isEmpty() ? null : result.get(0);
    }

    /**
     * Find all the cards in the hand with the frequency specified
     * @param hand player hand
     * @param frequency count to check
     * @return card value
     */
    public static List<CardValueEnum> findCardsWithFrequency(final PlayerHand hand, final int frequency) {
        final List<CardValueEnum> result = hand.getValuesWithCountMap().entrySet().stream().filter(t -> t.getValue().equals(frequency)).map(Map.Entry::getKey)
            .collect(Collectors.toList());
        return result;
    }

    /**
     * returns true if the map has a value with specified count
     * @param valuesMap values map
     * @param count count to look for
     * @return true if value found with the count specified; else false.
     */
    private static boolean hasValueCount(final Map<CardValueEnum, Integer> valuesMap, final int count) {
        return valuesMap.values().contains(count);
    }
}
