package com.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.factory.HandAnalyzerFactory;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;

/**
 * Program to find the winner in a five card poker game.
 */
public class FiveCardPoker {

    /** number of cards expected */
    private static final int CARD_COUNT = 5;

    public static void main(final String[] args) {
        System.out.println("Enter cards in the following format: 2D 4S 5H 6C AH. Note: D - Diamonds, S - Spades, C - Clubs, H - Hearts");

        try (final Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter Player1 Name: ");
            final String playerName1 = scanner.nextLine();

            System.out.println("Enter Player1 Cards seperated with space: ");
            final String playerCards1 = scanner.nextLine();

            System.out.println("Enter Player2 Name: ");
            final String playerName2 = scanner.nextLine();

            System.out.println("Enter Player2 Cards seperated with space: ");
            final String playerCards2 = scanner.nextLine();

            findWinner(playerName1, playerCards1, playerName2, playerCards2);
        }
    }

    public static GameResult findWinner(final String playerName1, final String playerCards1, final String playerName2, final String playerCards2) {
        final boolean inputsValid = isValid(playerName1) && isValid(playerCards1) && isValid(playerName2) && isValid(playerCards2);
        if (!inputsValid) {
            throw new IllegalArgumentException("Invalid inputs");
        }

        final PlayerHand playerHand1 = buildPlayerHand(playerName1, playerCards1);
        final PlayerHand playerHand2 = buildPlayerHand(playerName2, playerCards2);

        final HandAnalyzerFactory factory = new HandAnalyzerFactory();
        final GameResult result = factory.analyze(playerHand1, playerHand2);
        return result;

    }

    /**
     * Build each player.
     * @param playerName name of the player
     * @param cardsInput all the 5 player cards represented as a string
     * @return player hand
     */
    private static PlayerHand buildPlayerHand(final String playerName, final String cardsInput) {
        final List<String> playerCards = Arrays.asList(cardsInput.split(" "));
        if (playerCards.size() != CARD_COUNT) {
            final String message = String.format("Invalid number of cards: %s; expected %s cards", playerCards.size(), CARD_COUNT);
            throw new IllegalArgumentException(message);
        }
        final List<CardValueEnum> cardValues = new ArrayList<>();
        final List<CardSuitEnum> cardSymbols = new ArrayList<>();
        playerCards.forEach(s -> {
            final String[] arr = s.split("");
            CardValueEnum convertedValue = null;
            for (final CardValueEnum eachEnum : CardValueEnum.values()) {
                if (eachEnum.getFace().equals(arr[0])) {
                    convertedValue = eachEnum;
                    break;
                }
            }
            if (convertedValue == null) {
                throw new IllegalArgumentException("Invalid cardsInput for card value:" + arr[0]);
            }
            cardValues.add(convertedValue);

            cardSymbols.add(CardSuitEnum.valueOf(arr[1]));
        });

        final PlayerHand playerHand = new PlayerHand(playerName, cardValues, cardSymbols);
        return playerHand;
    }

    private static boolean isValid(final String str) {
        return str != null && str.length() > 0;
    }
}
