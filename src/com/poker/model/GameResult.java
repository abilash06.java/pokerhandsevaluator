package com.poker.model;

import com.poker.enumeration.CardValueEnum;
import com.poker.enumeration.PokerHandTypeEnum;

/**
 * Holds the details of the game result
 */
public class GameResult {

    /** the hand that won the game */
    private PlayerHand winningHand;
    /** the poker hand with which the player won the game */
    private PokerHandTypeEnum pokerHandType;
    /** represents if the game is a tie */
    private boolean isTie;
    /** represents the winning card for a tie breaker */
    private CardValueEnum highCard;

    /** Used to represent the result where there is no winner */
    public static final GameResult NO_WINNER = new GameResult();

    /**
     * Default constructor
     */
    private GameResult() {
        // prevent instantiation
    }

    /**
     * Constructor to build the result when there is a winner
     * @param winningHand winning hand
     * @param pokerHandType type of the poker hand
     */
    public GameResult(final PlayerHand winningHand, final PokerHandTypeEnum pokerHandType) {
        this.winningHand = winningHand;
        this.pokerHandType = pokerHandType;
        this.isTie = false;
    }

    /**
     * Constructor to build the result when there is a winner
     * @param winningHand winning hand
     * @param pokerHandType type of the poker hand
     * @param highCard winning card for a tie breaker
     */
    public GameResult(final PlayerHand winningHand, final PokerHandTypeEnum pokerHandType, final CardValueEnum highCard) {
        this(winningHand, pokerHandType);
        this.highCard = highCard;
    }

    /**
     * Constructor to build the result when the game is a tie
     * @param pokerHandType type of the poker hand
     * @param isTie true if tie
     */
    public GameResult(final PokerHandTypeEnum pokerHandType, final boolean isTie) {
        this.pokerHandType = pokerHandType;
        this.isTie = isTie;
    }

    public PlayerHand getWinningHand() {
        return winningHand;
    }

    public CardValueEnum getHighCard() {
        return highCard;
    }

    public PokerHandTypeEnum getPokerHandType() {
        return pokerHandType;
    }

    public boolean isTie() {
        return isTie;
    }

    @Override
    public String toString() {
        final String str;
        if (this.isTie) {
            str = "Game is a tie";
        } else {
            str = String.format("%s won the game with %s", this.getWinningHand().getPlayerName(), this.getPokerHandType());
        }
        return str;
    }
}
