package com.poker.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;

/**
 * Represents each player hand
 */
public class PlayerHand {

    /** name of the player */
    private final String playerName;
    /** list of the card values as enums */
    private final List<CardValueEnum> cardValues = new ArrayList<>();
    /** list of the card suits as enums */
    private final List<CardSuitEnum> cardSuits = new ArrayList<>();

    /** map of all the values with their count */
    private final Map<CardValueEnum, Integer> valuesWithCountMap = new TreeMap<>();
    /** map of all the suits with their count */
    private final Map<CardSuitEnum, Integer> suitsWithCountMap = new TreeMap<>();

    /**
     * Constructor to build the player hand
     * @param playerName name of the player
     * @param cardValues card values
     * @param cardSuits card suits
     */
    public PlayerHand(final String playerName, final List<CardValueEnum> cardValues, final List<CardSuitEnum> cardSuits) {
        this.playerName = playerName;
        this.cardValues.addAll(cardValues);
        this.cardSuits.addAll(cardSuits);

        // build the maps for later use.
        this.valuesWithCountMap.putAll(buildValuesCountMap());
        this.suitsWithCountMap.putAll(buildSymbolsCountMap());
    }

    public String getPlayerName() {
        return playerName;
    }

    public List<CardValueEnum> getCardValues() {
        return cardValues;
    }

    public List<CardSuitEnum> getCardSuits() {
        return cardSuits;
    }

    public Map<CardValueEnum, Integer> getValuesWithCountMap() {
        return valuesWithCountMap;
    }

    public Map<CardSuitEnum, Integer> getSuitsWithCountMap() {
        return suitsWithCountMap;
    }

    /**
     * Build a map with card values and the corresponding count
     * @return map with values and their counts
     */
    private Map<CardValueEnum, Integer> buildValuesCountMap() {
        final Map<CardValueEnum, Integer> cardValueCountMap = new TreeMap<>();
        for (final CardValueEnum eachValue : this.cardValues) {
            final Integer count = Collections.frequency(this.cardValues, eachValue);
            cardValueCountMap.putIfAbsent(eachValue, count);
        }
        return cardValueCountMap;
    }

    /**
     * Build a map with card suits and the corresponding count
     * @return map with suits and their counts
     */
    private Map<CardSuitEnum, Integer> buildSymbolsCountMap() {
        final Map<CardSuitEnum, Integer> cardSymbolCountMap = new TreeMap<>();
        for (final CardSuitEnum eachSymbol : this.cardSuits) {
            final Integer count = Collections.frequency(this.cardSuits, eachSymbol);
            cardSymbolCountMap.putIfAbsent(eachSymbol, count);
        }
        return cardSymbolCountMap;
    }

}
