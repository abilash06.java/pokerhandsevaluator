package com.poker.model;

import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;

/**
 * Represent each playing card
 */
public class Card {

    /** card value as enum */
    private final CardValueEnum cardValue;
    /** card suit as enum */
    private final CardSuitEnum cardSuit;

    /**
     * Constructor to build the card
     * @param cardValue value of card
     * @param cardSuit suit of card
     */
    public Card(final CardValueEnum cardValue, final CardSuitEnum cardSuit) {
        this.cardValue = cardValue;
        this.cardSuit = cardSuit;
    }

    public CardValueEnum getCardValue() {
        return cardValue;
    }

    public CardSuitEnum getCardSuit() {
        return cardSuit;
    }

}
