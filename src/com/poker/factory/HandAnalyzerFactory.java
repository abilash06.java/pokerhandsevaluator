package com.poker.factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.poker.analyzers.FlushAnalyzer;
import com.poker.analyzers.FourOfAKindAnalyzer;
import com.poker.analyzers.FullHouseAnalyzer;
import com.poker.analyzers.HandAnalyzer;
import com.poker.analyzers.HighCardAnalyzer;
import com.poker.analyzers.PairAnalyzer;
import com.poker.analyzers.StraightAnalyzer;
import com.poker.analyzers.StraightFlushAnalyzer;
import com.poker.analyzers.ThreeOfAKindAnalyzer;
import com.poker.analyzers.TwoPairsAnalyzer;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;

/**
 * Uses a list of analyzers to determine the winning hand along with it's the poker hand type.
 */
public final class HandAnalyzerFactory {

    public static final List<HandAnalyzer> ALL_ANALYZERS;

    static {
        final List<HandAnalyzer> tmpList = new ArrayList<>();
        tmpList.add(new StraightFlushAnalyzer());
        tmpList.add(new FourOfAKindAnalyzer());
        tmpList.add(new FullHouseAnalyzer());
        tmpList.add(new FlushAnalyzer());
        tmpList.add(new HighCardAnalyzer());
        tmpList.add(new StraightAnalyzer());
        tmpList.add(new ThreeOfAKindAnalyzer());
        tmpList.add(new TwoPairsAnalyzer());
        tmpList.add(new PairAnalyzer());

        tmpList.sort(Comparator.comparingInt(o -> o.getPokerHandType().getRank()));
        ALL_ANALYZERS = Collections.unmodifiableList(tmpList);
    }

    /**
     * Analyze both the input hands using all the analyzers configured in the factory and determine the winning hand
     * @param hand1 player1 hand
     * @param hand2 player2 hand
     * @return game result with winning hand and type of the poker hand
     */
    public GameResult analyze(final PlayerHand hand1, final PlayerHand hand2) {
        GameResult gameResult = GameResult.NO_WINNER;
        for (final HandAnalyzer eachAnalyzer : ALL_ANALYZERS) {
            gameResult = eachAnalyzer.determineGameResult(hand1, hand2);
            if (!gameResult.equals(GameResult.NO_WINNER)) {
                break;
            }
        }
        return gameResult;
    }
}
