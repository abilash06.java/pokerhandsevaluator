package com.poker.enumeration;

/**
 * Enumerated values of poker hand types
 */
public enum PokerHandTypeEnum {

    STRAIGHT_FLUSH(1, "straight flush"),
    FOUR_OF_A_KIND(2, "four of a kind"),
    FULL_HOUSE(3, "full house"),
    FLUSH(4, "flush"),
    STRAIGHT(5, "straight"),
    THREE_OF_A_KIND(6, "three of a kind"),
    TWO_PAIRS(7, "two pairs"),
    PAIR(8, "pair"),
    HIGH_CARD(9, "high card");

    private final int rank;
    private final String name;

    PokerHandTypeEnum(final int rank, final String name) {
        this.rank = rank;
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public String getName() {
        return name;
    }
}
