package com.poker.enumeration;

/**
 * Enumerated values for card suits
 */
public enum CardSuitEnum {

    S, // spade
    H, // heart
    D, // diamond
    C  // club
}
