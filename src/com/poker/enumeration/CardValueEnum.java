package com.poker.enumeration;

/**
 * Enumerated values for card face values
 */
public enum CardValueEnum {

    TWO("2", 2),
    THREE("3", 3),
    FOUR("4", 4),
    FIVE("5", 5),
    SIX("6", 6),
    SEVEN("7", 7),
    EIGHT("8", 8),
    NINE("9", 9),
    TEN("T", 10),
    JACK("J", 11),
    QUEEN("Q", 12),
    KING("K", 13),
    ACE("A", 14);

    /** face value of the card */
    private final String face;
    /** numeric value of the card */
    private final int value;

    /**
     * constructor to build the enum
     * @param face face of the card
     * @param value numeric value of the card
     */
    CardValueEnum(final String face, final int value) {
        this.face = face;
        this.value = value;
    }

    /**
     * Get card numeric value
     * @return card numeric value
     */
    public int getValue() {
        return value;
    }

    /**
     * Get card face value
     * @return card face value
     */
    public String getFace() {
        return face;
    }

}
