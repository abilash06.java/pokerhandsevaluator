package com.poker.analyzer;

import java.util.Arrays;
import java.util.List;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static com.poker.enumeration.CardSuitEnum.C;
import static com.poker.enumeration.CardSuitEnum.D;
import static com.poker.enumeration.CardSuitEnum.H;
import static com.poker.enumeration.CardSuitEnum.S;
import static com.poker.enumeration.CardValueEnum.EIGHT;
import static com.poker.enumeration.CardValueEnum.FIVE;
import static com.poker.enumeration.CardValueEnum.FOUR;
import static com.poker.enumeration.CardValueEnum.KING;
import static com.poker.enumeration.CardValueEnum.TEN;
import static com.poker.enumeration.CardValueEnum.THREE;
import static org.junit.Assert.assertEquals;

import com.poker.analyzers.FourOfAKindAnalyzer;
import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import org.junit.Test;

public class FourOfAKindAnalyzerTest {

    @Test
    public void testPlayer1FourKind() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(FIVE, EIGHT, FIVE, FIVE, FIVE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(KING, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, D, D, D, D);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FourOfAKindAnalyzer fa = new FourOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2FourKind() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(FIVE, EIGHT, FIVE, FOUR, FIVE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(KING, TEN, TEN, TEN, TEN);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FourOfAKindAnalyzer fa = new FourOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }
}
