package com.poker.analyzer;

import java.util.Arrays;
import java.util.List;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static com.poker.enumeration.CardSuitEnum.D;
import static com.poker.enumeration.CardSuitEnum.H;
import static com.poker.enumeration.CardSuitEnum.S;
import static com.poker.enumeration.CardValueEnum.EIGHT;
import static com.poker.enumeration.CardValueEnum.FIVE;
import static com.poker.enumeration.CardValueEnum.FOUR;
import static com.poker.enumeration.CardValueEnum.JACK;
import static com.poker.enumeration.CardValueEnum.KING;
import static com.poker.enumeration.CardValueEnum.NINE;
import static com.poker.enumeration.CardValueEnum.QUEEN;
import static com.poker.enumeration.CardValueEnum.SIX;
import static com.poker.enumeration.CardValueEnum.TEN;
import static com.poker.enumeration.CardValueEnum.THREE;
import static com.poker.enumeration.CardValueEnum.TWO;
import static org.junit.Assert.assertEquals;

import com.poker.analyzers.StraightFlushAnalyzer;
import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import org.junit.Test;

public class StraightFlushAnalyzerTest {

    @Test
    public void testPlayer1StraightFlush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, FIVE, FOUR, TWO, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(S, S, S, S, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, KING, TEN, TEN, TEN);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final StraightFlushAnalyzer fa = new StraightFlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2StraightFlush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, FIVE, FOUR, TWO, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(S, S, S, S, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, KING, JACK, QUEEN, NINE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final StraightFlushAnalyzer fa = new StraightFlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testTieStraightFlush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, FIVE, FOUR, TWO, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(S, S, S, S, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(SIX, FIVE, FOUR, TWO, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final StraightFlushAnalyzer fa = new StraightFlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(true, wa.isTie());
    }

    @Test
    public void testPlayer1StraightFlushWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(TEN, KING, JACK, QUEEN, NINE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(S, S, S, S, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, EIGHT, JACK, QUEEN, NINE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final StraightFlushAnalyzer fa = new StraightFlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2StraightFlushWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, FIVE, FOUR, TWO, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(S, S, S, S, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, KING, JACK, QUEEN, NINE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final StraightFlushAnalyzer fa = new StraightFlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }
}
