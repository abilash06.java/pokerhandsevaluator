package com.poker.analyzer;

public final class TestConstants {

    public static final String PLAYER_1_NAME = "TestPlayerOne";

    public static final String PLAYER_2_NAME = "TestPlayerTwo";
}
