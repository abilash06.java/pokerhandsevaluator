package com.poker.analyzer;

import java.util.Arrays;
import java.util.List;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static com.poker.enumeration.CardSuitEnum.C;
import static com.poker.enumeration.CardSuitEnum.D;
import static com.poker.enumeration.CardSuitEnum.H;
import static com.poker.enumeration.CardSuitEnum.S;
import static com.poker.enumeration.CardValueEnum.ACE;
import static com.poker.enumeration.CardValueEnum.EIGHT;
import static com.poker.enumeration.CardValueEnum.FIVE;
import static com.poker.enumeration.CardValueEnum.FOUR;
import static com.poker.enumeration.CardValueEnum.KING;
import static com.poker.enumeration.CardValueEnum.SIX;
import static com.poker.enumeration.CardValueEnum.TEN;
import static junit.framework.TestCase.assertEquals;

import com.poker.analyzers.ThreeOfAKindAnalyzer;
import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import org.junit.Test;

public class ThreeOfAKindAnalyzerTest {

    @Test
    public void testPlayer1ThreeOfAKind() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, SIX, FOUR, SIX, EIGHT);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(FIVE, KING, FIVE, EIGHT, KING);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final ThreeOfAKindAnalyzer fa = new ThreeOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2ThreeOfAKind() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, SIX, FOUR, SIX, EIGHT);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, KING, FIVE, KING, KING);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final ThreeOfAKindAnalyzer fa = new ThreeOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer1ThreeOfAKindWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, SIX, FOUR, SIX, EIGHT);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, FIVE, FIVE, EIGHT, FIVE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final ThreeOfAKindAnalyzer fa = new ThreeOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2ThreeOfAKindWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, SIX, FOUR, SIX, EIGHT);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, KING, FIVE, KING, KING);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final ThreeOfAKindAnalyzer fa = new ThreeOfAKindAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }
}
