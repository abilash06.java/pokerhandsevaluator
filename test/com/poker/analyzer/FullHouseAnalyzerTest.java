package com.poker.analyzer;

import java.util.Arrays;
import java.util.List;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static com.poker.enumeration.CardSuitEnum.C;
import static com.poker.enumeration.CardSuitEnum.D;
import static com.poker.enumeration.CardSuitEnum.H;
import static com.poker.enumeration.CardSuitEnum.S;
import static com.poker.enumeration.CardValueEnum.EIGHT;
import static com.poker.enumeration.CardValueEnum.FIVE;
import static com.poker.enumeration.CardValueEnum.FOUR;
import static com.poker.enumeration.CardValueEnum.KING;
import static com.poker.enumeration.CardValueEnum.QUEEN;
import static com.poker.enumeration.CardValueEnum.SEVEN;
import static com.poker.enumeration.CardValueEnum.SIX;
import static com.poker.enumeration.CardValueEnum.TEN;
import static org.junit.Assert.assertEquals;

import com.poker.analyzers.FullHouseAnalyzer;
import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import org.junit.Test;

public class FullHouseAnalyzerTest {

    @Test
    public void testPlayer1FullHouse() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, SIX, QUEEN, SIX, QUEEN);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, FOUR, TEN, EIGHT, FOUR);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FullHouseAnalyzer fa = new FullHouseAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2FullHouse() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, FIVE, QUEEN, SIX, SIX);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, FOUR, TEN, FOUR, FOUR);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FullHouseAnalyzer fa = new FullHouseAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer1FullHouseHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SIX, SIX, QUEEN, SIX, QUEEN);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(TEN, FOUR, TEN, FOUR, FOUR);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FullHouseAnalyzer fa = new FullHouseAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2FullHouseHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(SEVEN, SIX, QUEEN, SIX, SIX);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, C, H, D, S);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(KING, KING, KING, SEVEN, SEVEN);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, D, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FullHouseAnalyzer fa = new FullHouseAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }

}
