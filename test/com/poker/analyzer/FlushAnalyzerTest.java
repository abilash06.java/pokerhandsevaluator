package com.poker.analyzer;

import java.util.Arrays;
import java.util.List;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static com.poker.enumeration.CardSuitEnum.D;
import static com.poker.enumeration.CardSuitEnum.H;
import static com.poker.enumeration.CardSuitEnum.S;
import static com.poker.enumeration.CardValueEnum.ACE;
import static com.poker.enumeration.CardValueEnum.EIGHT;
import static com.poker.enumeration.CardValueEnum.FIVE;
import static com.poker.enumeration.CardValueEnum.FOUR;
import static com.poker.enumeration.CardValueEnum.KING;
import static com.poker.enumeration.CardValueEnum.TEN;
import static com.poker.enumeration.CardValueEnum.THREE;
import static org.junit.Assert.assertEquals;

import com.poker.analyzers.FlushAnalyzer;
import com.poker.enumeration.CardSuitEnum;
import com.poker.enumeration.CardValueEnum;
import com.poker.model.GameResult;
import com.poker.model.PlayerHand;
import org.junit.Test;

public class FlushAnalyzerTest {

    @Test
    public void testPlayer1Flush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, D, D, D, D);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(KING, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(D, H, S, D, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FlushAnalyzer fa = new FlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_1_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2Flush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(H, D, S, D, D);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(KING, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FlushAnalyzer fa = new FlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testTieFlush() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, D, D, D, D);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FlushAnalyzer fa = new FlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(true, wa.isTie());
    }

    @Test
    public void testPlayer1FlushWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, FOUR);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, D, D, D, D);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FlushAnalyzer fa = new FlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(playerNameOne, wa.getWinningHand().getPlayerName());
    }

    @Test
    public void testPlayer2FlushWithHighCard() {

        final String playerNameOne = PLAYER_1_NAME;
        final List<CardValueEnum> playerOneCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, THREE);
        final List<CardSuitEnum> playerOneCardSuits = Arrays.asList(D, D, D, D, D);

        final String playerNameTwo = PLAYER_2_NAME;
        final List<CardValueEnum> playerTwoCardValues = Arrays.asList(ACE, EIGHT, FIVE, TEN, FOUR);
        final List<CardSuitEnum> playerTwoCardSuits = Arrays.asList(H, H, H, H, H);

        final PlayerHand hand1 = new PlayerHand(playerNameOne, playerOneCardValues, playerOneCardSuits);
        final PlayerHand hand2 = new PlayerHand(playerNameTwo, playerTwoCardValues, playerTwoCardSuits);

        final FlushAnalyzer fa = new FlushAnalyzer();
        final GameResult wa = fa.determineGameResult(hand1, hand2);

        assertEquals(PLAYER_2_NAME, wa.getWinningHand().getPlayerName());
    }
}
