package com.poker;

import static com.poker.analyzer.TestConstants.PLAYER_1_NAME;
import static com.poker.analyzer.TestConstants.PLAYER_2_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.poker.enumeration.PokerHandTypeEnum;
import com.poker.model.GameResult;
import org.junit.Test;

public class FiveCardPokerTest {

	@Test
	public void testFlush() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "2H 4H 5H 6H AH";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "2S 4S 5S KS AS";

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.FLUSH);
	}
	
	@Test
	public void testStraightFlush() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "2H 4H 3H 6H 5H";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "6S 4S 5S 7S 8S";
		

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.STRAIGHT_FLUSH);
	}
	
	@Test
	public void testFourOfAKIND() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 4S 5C 5S 5D";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "9S 9C 9H 9D 8S";
		

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.FOUR_OF_A_KIND);
	}
	
	@Test
	public void testFullHouse() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 4S 5C 4S 4D";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "3S 3C 3H 2D 2S";
		

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player1Name, PokerHandTypeEnum.FULL_HOUSE);
	}
	
	@Test
	public void testStraight() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 6S 7C 9S 8D";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C 8H JD QS";
		

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.STRAIGHT);
	}
	
	@Test
	public void testThreeOfAKind() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 6S 7C 7S 7D";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C 3H 3D 3S";
		

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player1Name, PokerHandTypeEnum.THREE_OF_A_KIND);
	}
	
	@Test
	public void testTwoPairs() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 6S 7C 7S 6D";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C 3H 3D TS";	

		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.TWO_PAIRS);
	}
	
	@Test
	public void testPair() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 6S 7C 7S TD";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C 9H 3D JS";
		
		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player2Name, PokerHandTypeEnum.PAIR);
	}
	
	@Test
	public void testHighCard() {
		
		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H 6S AC 7S TD";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C KH 3D JS";
		
		performTestAndVerifyResult(player1Name, player1Cards, player2Name, player2Cards, player1Name, PokerHandTypeEnum.HIGH_CARD);
	}

	@Test
	public void testLessNumberOfCards() {

		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H AC 7S TD";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS 9C KH 3D JS";

		try {
			FiveCardPoker.findWinner(player1Name, player1Cards, player2Name, player2Cards);
			fail("Expected to throw and exception");
		} catch (IllegalArgumentException ex) {
			assertEquals("Invalid number of cards: 4; expected 5 cards", ex.getMessage());
		}
	}

	@Test
	public void testIncorrectCardValue() {

		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H AC 3C 7S TD";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS LC KH 3D JS";

		try {
			FiveCardPoker.findWinner(player1Name, player1Cards, player2Name, player2Cards);
			fail("Expected to throw and exception");
		} catch (IllegalArgumentException ex) {
			assertEquals("Invalid cardsInput for card value:L", ex.getMessage());
		}
	}

	@Test
	public void testIncorrectCardSuit() {

		final String player1Name = PLAYER_1_NAME;
		final String player1Cards = "5H AC 3C 7W TD";
		final String player2Name = PLAYER_2_NAME;
		final String player2Cards = "TS LC 5H 3D JS";

		try {
			FiveCardPoker.findWinner(player1Name, player1Cards, player2Name, player2Cards);
			fail("Expected to throw and exception");
		} catch (IllegalArgumentException ex) {
			assertEquals("No enum constant com.poker.enumeration.CardSuitEnum.W", ex.getMessage());
		}
	}
	
	private static void performTestAndVerifyResult(final String player1Name, final String player1Cards, final String player2Name, final String player2Cards,
		final String expectedWinnerName, final PokerHandTypeEnum expectedPokerHandType) {
		
		final GameResult result = FiveCardPoker.findWinner(player1Name, player1Cards, player2Name, player2Cards);
		
		assertEquals(expectedWinnerName, result.getWinningHand().getPlayerName());
		assertEquals(expectedPokerHandType, result.getPokerHandType());
	}
	
}
